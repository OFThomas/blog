# Hi

[See the docs!](https://blog.oli.computer/)

You need to add documentation to `docs/index.md`.

Locally requires
```
pip install mkdocs mkdocs-material pymdown-extensions mkdocs-mermaid2-plugin mkdocs-blogging-plugin
```

Test locally with 
```
mkdocs serve
```
