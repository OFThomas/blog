---
title: How to Setup a Blog
description: If you've ever wanted to setup a blog using mkdocs and git, this is the guide for you. Mainly me.
author: Oli
tags:
  - mkdocs
  - git
  - linux
  - fresh install
  - where are all my notes
---

# How to Set up a Blog

## Install requirements / TLDR

```bash
sudo apt update
sudo apt install mkdocs
pip install mkdocs-material mkdocs-blogging-plugin
```

# A Journey 

So the first thing you might do is run 
```bash
mkdocs serve
```
I was first greeted by this error:
```bash
oli@pop-os:~/Documents/blog$ mkdocs serve
Command 'mkdocs' not found, but can be installed with:
sudo apt install mkdocs
``` 

Nice it even has the command to install it. So I ran that and got this error:

```bash
oli@pop-os:~/Documents/blog$ mkdocs serve
INFO    -  Building documentation... 
ERROR   -  Config value: 'theme'. Error: Unrecognised theme name: 'material'. The available installed themes are: mkdocs, readthedocs 
ERROR   -  Config value: 'markdown_extensions'. Error: Failed loading extension "pymdownx.arithmatex". 
ERROR   -  Config value: 'plugins'. Error: The "blogging" plugin is not installed 

Aborted with 3 Configuration Errors!
```

So the procedure is now to always start with the first error, as sometimes later
errors can just be side effects of earlier errors. The thing to fix is:

## Themes

1. Install the material theme

The [website](https://github.com/squidfunk/mkdocs-material) gives:

```bash
pip install mkdocs-material
``` 
Trying `mkdocs serve` again produces:
```bash
ERROR    -  Config value 'plugins': The "blogging" plugin is not installed
Aborted with 1 Configuration Errors!
```
Are you feeling lucky? Let's
[try](https://pypi.org/project/mkdocs-blogging-plugin/)

```bash
pip install mkdocs-blogging-plugin
```
Oh my God, it actually ran successfully this time (`mkdocs serve`).

## Thoughts

To be honest this was way easier than the first time I set this up. I'm not sure
what went so horribly wrong the first time, but this was actually pretty easy.

# Setting up CI

There is one more (optional) step. I have (probably stolen from somewhere) a
gitlab ci script that on pushes to the main branch runs the tooling. It's at the
root of the repository at [.gitlab-ci.yml](https://gitlab.com/OFThomas/blog/-/blob/main/.gitlab-ci.yml?ref_type=heads).
It is short, so I'm just going to paste it here as well.
```bash
image: python:3.8-buster

before_script:
  - pip install mkdocs mkdocs-material pymdown-extensions mkdocs-mermaid2-plugin mkdocs-blogging-plugin
test:
  stage: test
  script:
  - mkdocs build --strict --verbose --site-dir test
  artifacts:
    paths:
    - test
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH

pages:
  stage: deploy
  script:
  - mkdocs build --strict --verbose
  artifacts:
    paths:
    - public
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```
