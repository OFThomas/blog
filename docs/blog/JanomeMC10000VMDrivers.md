---
title: Using a machine that only works on Windows XP.
description: How get get software written for a 32bit OS in written 2006 to work on Linux in 2023.
author: Oli
tags: 
  - Janome
  - Sewing Machine
  - Windows XP
  - Virtual Machine
---

# Using a machine that only works on Windows XP.

Imagine yourself in this situation:

- I have a Janome MC10000 sewing machine.
- It only works with Windows XP.
- I don't have a Windows XP machine.
- I don't want to buy a Windows XP machine.
- I don't want to buy a new sewing machine.


This bad boy can fit so many patterns in it.

![AltText](./images/JanomeMC10000/1012a.png)


# The solution

## TLDR 

Use a virtual machine, install Windows XP and the drivers for the sewing
machine. Then use a USB to serial adapter to connect the sewing machine to the virtual machine. 

## The long version

So these instructions are going to be for PopOS 21.04, but they should work
for recent versions of Ubuntu and other Debian based distros with little to
no modification. However, most of the setup is done in the virtual machine
so it should work on any host OS.

### Install VirtualBox

You can use the PopShop, 
![](./images/JanomeMC10000/popShop.png)
And search for `virtualbox`, (which for me there is only one) and install it. 
![](./images/JanomeMC10000/popVirtualBox.png)
Or you can use the command line:
```
sudo apt update
sudo apt install virtualbox
```

### Download Windows XP
You'll need a Windows XP ISO, which is a disk image file (ending in `.iso`).
You can download one from [here](https://archive.org/details/WinXPProSP3x86).

### Create a new virtual machine

When you open VirtualBox you should see a screen like this:
![](./images/JanomeMC10000/virtualbox.png)

Click on `New` and you should see a screen like this:
![](./images/JanomeMC10000/virtualboxNew.png)

You can just click through next and use the default values for everything.

### Install Windows XP on the virtual machine

Now you should see your new virtual machine in the list on the left. Click on
it and then click on `Start`. You should see a screen like this:
![](./images/JanomeMC10000/virtualboxStart.png)

You'll need to select the Windows XP ISO you downloaded earlier. Click on the folder icon and then navigate to the ISO file. Then click on `Start`.

Nice! 

Now the important part is not to press keys unless you're in the windows
installer. If you do, you'll keep trying to boot the VM from a CD drive
that isn't there, so just wait until a Windows screen appears!

## Using windows 
![](./images/JanomeMC10000/windowsXP.png)


### Install the drivers
So now we need to install the sewing machine drivers onto our virtual
machine.

Basically you need to rip the CD to an ISO file and then you can mount that
ISO file in the virtual machine, basically the same as installing windows.

#### Rip the CD to an ISO file

On PopOS / Ubuntu the `disks` tool is install by default, you can create a
disk image like so:
![](./images/JanomeMC10000/disks.png)

And then
![](./images/JanomeMC10000/disksImage.png)

#### Mount the ISO file in the virtual machine

In the top menu bar of the virtual machine window, click on `Devices` and
then `Optical Drives` and then `Choose a disk file...` and then navigate to the ISO file you just made.

#### Install the drivers
Then in the virtual machine you can open `my computer` and then open the CD
(which is the ISO file you just mounted).

Like so ![](./images/JanomeMC10000/windowsCD.png)

Then you can run the installer and install the drivers.
![](./images/JanomeMC10000/windowsSetupCD.png)

## VirtualBox extensions

Of course there is always *one more thing*. By default virtualbox doesn't
support USB 2.0, which is what we need to connect the sewing machine to the virtual machine.

So we need to install the VirtualBox extensions. You can download them from [here](https://download.virtualbox.org/virtualbox/6.1.26/Oracle_VM_VirtualBox_Extension_Pack-6.1.26.vbox-extpack).

Then you can install the extensions in the virtual machine. This
[website](https://phoenixnap.com/kb/install-virtualbox-extension-pack) has
pictures if you get stuck.


