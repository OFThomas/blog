---
title: RhoQuantum - The Arm powered Quantum Computer simulator
description: Based on the huge success of the first version (PiQuantum) we're back with even more LEDs!
author: Oli
tags:
  - Quantum
  - Quantum computing
  - Arm
  - Programming
  - Embedded C
  - Fun?
---

# RhoQuantum the world's ___best___ quantum computer simulator

So the project is currently still under active development, but John has
finished revision 1 of the prototype! The project is hosted on gitlab,
[RhoQuantum](https://gitlab.com/piquantum/rhoquantum).

It looks like this at the moment: ![picture of rhoquantum prototype board](https://gitlab.com/piquantum/rhoquantum/-/raw/main/photo-rev1.0.png)

Basically there are 19 'Qubits' arranged in a hexagon in the centre-right of the
board, they each have 1 (incredibly bright) LED, which we are using to represent
the quantum state of the qubit.

The code to model quantum side of the project is written in very simple `C` at
the moment, it's based on the rather more complex C++ code that we used in the
first version of this project that ran on a Raspberry Pi,
(PiQuantum)[https://gitlab.com/piquantum/PiQuantum]

I'll add some more info and maybe a demo when I fix all the bugs. There are
many.

# PiQuantum - The original

I don't think we ever wrote about our (mine and John's) work on PiQuantum, which
I think is a shame, so here you go I will mention and link to it.

I had to put the KICAD render of PiQuantum here because it looks cool! ![picture of
piquantum](https://gitlab.com/piquantum/PiQuantum/-/raw/master/res/cad.png)
I actually don't think we have any finished pictures of PiQuantum, it's now
residing at the University of Bristol (somewhere).
